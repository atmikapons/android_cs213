package com.example.user.android05;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;


/**
 * @author Atmika Ponnusamy and Audri Yoon
 */
public class AddAlbumActivity extends AppCompatActivity {
    private Button add;
    private EditText name;
    private List<Album> albums = MainActivity.albums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_album);

        add = (Button) findViewById(R.id.add);
        name = (EditText) findViewById(R.id.album_name);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter a name");
                } else if(MainActivity.hasAlbum(name.getText().toString().trim())){
                    name.setError("Album already exists. Enter a unique name.");
                } else {
                    Album newAlb = new Album(name.getText().toString().trim());
                    System.out.println("before " + albums);
                    albums.add(newAlb);
                    System.out.println("after " + albums);

                    Toast.makeText(getApplicationContext(),
                            "Album Added!", Toast.LENGTH_SHORT).show();

                    MainActivity.updateAdapter();
                    MainActivity.writeApp();

                    name.setText("");

                    Intent i = new Intent(AddAlbumActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });
    }
}




