package com.example.user.android05;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Atmika Ponnusamy and Audri Yoon
 */
public class Album implements Serializable {

    private static final long serialVersionUID = -4981047832242342644L;
    private String name;
    private List<Photo> photos = new ArrayList<Photo>();

    public Album(String name) {
        this.name = name;
    }

    public Album(String name, List<Photo> photos) {
        this.name = name;
        this.photos = photos;
    }


    /**Sets the name of the album.
     * @param str takes a String representing the album's name
     */
    public void setName(String str) {
        name = str;
    }

    /**
     * @return the Album's name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the List of Photos associated with an Album
     */
    public List<Photo> getPhotoList() {
        return photos;
    }

    /**
     * @param p list of Photo objects
     */
    public void setPhotosList(List<Photo> p) {
        photos = p;
    }

    /**Adds a photo to the Album's list of Photos.
     * @param photo object to add
     */
    public void addPhoto(Photo photo) {
        photos.add(photo);
    }

    /**Deletes a photo from the Album's list of Photos.
     * @param photo object to delete
     */
    public void deletePhoto(Photo photo) {
        photos.remove(photo);
    }

    /**
     *
     * @param cap caption of photo
     * @return true if album already contains that photo
     */
    public boolean duplicatePhoto(String cap) {
        for(Photo p : photos) {
            if(p.getCaption().equalsIgnoreCase(cap)) return true;
        }
        return false;
    }

    public String toString(){
        return name;
    }



}
