package com.example.user.android05;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * @author: Atmika Ponnusamy and Audri Yoon
 */

public class AlbumContentsActivity extends AppCompatActivity {

    private List<Album> albums = MainActivity.albums;
    private List<Photo> photos;
    private ListView listView;
    private static ArrayAdapter<Photo> arrayAdapter;
    private FloatingActionButton addPhoto;
    private TextView albumName;
    private static CustomAdapter customAdapter;
    private int albumIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_contents);

        Bundle b = getIntent().getExtras();
        albumIndex = b.getInt("album");
        photos = albums.get(albumIndex).getPhotoList();

        albumName = (TextView)findViewById(R.id.album_name);
        albumName.setText(getString(R.string.selected_album)
                + " " + albums.get(b.getInt("album")).getName());

        addPhoto = (FloatingActionButton)findViewById(R.id.add_photo) ;
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, 7);
            }
        });

        listView = findViewById(R.id.listview);
        customAdapter = new CustomAdapter(photos, getApplicationContext());
        listView.setAdapter(customAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //open albumContents
                Toast.makeText(getApplicationContext(),
                        "Photo Selected!", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AlbumContentsActivity.this,
                        PhotoDisplayActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", albumIndex);
                b.putInt("photo", i);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                //open activity to rename and delete album
//                Toast.makeText(getApplicationContext(),
//                        "Index Selected: " + pos
//                                + "\nAlbum Selected: " + albums.get(pos),
//                        Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AlbumContentsActivity.this,
                        MoveCopyRenameDeletePhotoActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", albumIndex);
                b.putInt("index", pos);
                i.putExtras(b);
                startActivity(i);
                return true;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case 7:
                if(resultCode==RESULT_OK){
                    String PathHolder = data.getData().getPath();
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory
                                .decodeStream(getContentResolver().openInputStream(data.getData()));

                        Photo p = new Photo(bitmap, data);

//                        Bitmap bitmap2 = BitmapFactory.decodeFile(data.getDataString());
//                        ByteArrayOutputStream blob = new ByteArrayOutputStream();
//                        bitmap2.compress(Bitmap.CompressFormat.PNG, 0 /* Ignored for PNGs */, blob);
//                        byte[] bitmapdata = blob.toByteArray();
//
//                        Photo p = new Photo(bitmapdata, data);

                        if(albums.get(albumIndex).duplicatePhoto(p.getCaption())){
                            Toast.makeText(getApplicationContext(),
                                    "Photo Already Exists Here!", Toast.LENGTH_SHORT).show();
                        } else {
                            photos.add(p);
                            updateAdapter();
                            MainActivity.writeApp();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    class CustomAdapter extends ArrayAdapter<Photo> implements View.OnClickListener {

        private List<Photo> dataSet;
        private ImageView imageView;
        private TextView textView;
        Context mContext;

        public CustomAdapter(List<Photo> photos, Context context) {
            super(context, R.layout.photo_list_item, photos);
            dataSet = photos;
            mContext = context;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Log.d("INDEX", "index = " + i + " caption = " + photos.get(i).getFilepath());

            View v = view;
            if(v == null) v = LayoutInflater.from(mContext)
                    .inflate(R.layout.photo_list_item, viewGroup,false);
            Photo p = dataSet.get(i);

            imageView = (ImageView) v.findViewById(R.id.imageview);
            //byte[] bitmapdata = p.getImage();
            imageView.setImageBitmap(p.getBitmap());

            textView = (TextView) v.findViewById(R.id.textview);
            textView.setText(p.getCaption());

            return v;
        }

        @Override
        public void onClick(View view) {}
    }

    public static void updateAdapter() {
        customAdapter.notifyDataSetChanged();
    }
}
