package com.example.user.android05;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Atmika Ponnusamy and Audri Yoon
 */
public class EditTagsActivity extends AppCompatActivity {

    private List<Album> albums = MainActivity.albums;
    private List<Photo> photos;
    private List<Tag> tags;
    private int selectedAlbum;
    private int selectedPhoto;
    private Button add;
    private Button delete;
    private Spinner typesSpinner;
    private Spinner tagsSpinner;
    private EditText newTag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_tags);

        Bundle b = getIntent().getExtras();
        selectedAlbum = b.getInt("album");
        photos = albums.get(selectedAlbum).getPhotoList();
        selectedPhoto = b.getInt("photo");
        tags = photos.get(selectedPhoto).getTags();

        typesSpinner = (Spinner) findViewById(R.id.type_spinner);

        newTag = (EditText) findViewById(R.id.new_tag);

        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(newTag.getText().toString())) {
                    newTag.setError("Enter a name");
                } else if(tags.contains(new Tag(typesSpinner.getSelectedItem().toString(),
                        newTag.getText().toString().toLowerCase()))){
                    newTag.setError("Album already exists. Enter a unique name.");
                } else {
                    tags.add(new Tag(typesSpinner.getSelectedItem().toString(),
                            newTag.getText().toString().toLowerCase()));

                    newTag.setText("");

                    MainActivity.writeApp();

                    Intent i = new Intent(EditTagsActivity.this,
                            PhotoDisplayActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("album", selectedAlbum);
                    b.putInt("photo", selectedPhoto);
                    i.putExtras(b);
                    startActivity(i);
                }
            }
        });

        tagsSpinner = (Spinner) findViewById(R.id.delete_tag_spinner);
        ArrayAdapter<Tag> adapter2 = new ArrayAdapter<Tag>(this.getApplicationContext(),
                android.R.layout.simple_spinner_item, tags);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tagsSpinner.setAdapter(adapter2);

        delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tags.remove((Tag)tagsSpinner.getSelectedItem());

                MainActivity.writeApp();

                Intent i = new Intent(EditTagsActivity.this,
                        PhotoDisplayActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                b.putInt("photo", selectedPhoto);
                i.putExtras(b);
                startActivity(i);
            }
        });

    }
}