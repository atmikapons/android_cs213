package com.example.user.android05;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * @author: Atmika Ponnusamy and Audri Yoon
 */
public class MainActivity extends AppCompatActivity {

    public static List<Album> albums;
    private static ArrayAdapter<Album> arrayAdapter;
    private ListView listView;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private FloatingActionButton search;
    private static File listData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        albums = new ArrayList<>();

        listData = new File(MainActivity.this.getFilesDir(), "albumList.dat");
//        if(!listData.exists()){
//            writeApp();
//        }
        readApp();
        System.out.println(albums);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<Album>(this.getApplicationContext(),
                android.R.layout.simple_list_item_1, albums);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //open albumContents
                Toast.makeText(getApplicationContext(),
                        "Album Selected!", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this,
                        AlbumContentsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", i);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                //open activity to rename and delete album
//                Toast.makeText(getApplicationContext(),
//                        "Index Selected: " + pos
//                                + "\nAlbum Selected: " + albums.get(pos),
//                        Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this,
                        RenameDeleteAlbumActivity.class);
                Bundle b = new Bundle();
                b.putInt("index", pos);
                i.putExtras(b);
                startActivity(i);
                return true;
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddAlbumActivity.class);
                startActivity(i);
                System.out.println(albums);
            }
        });

        search = (FloatingActionButton) findViewById(R.id.fab_search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void updateAdapter() {
        arrayAdapter.notifyDataSetChanged();
    }

    public static void writeApp(){
        try{
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(listData));
            oos.writeObject(albums);
            oos.close();
            Log.d("'", "onCreate: writeApp worked");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readApp(){
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(listData));
            albums = (ArrayList<Album>)ois.readObject();
            ois.close();
            Log.d("readApp", "onCreate: readApp worked");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean hasAlbum(String s){
        for(Album alb:albums){
            if(alb.getName().equalsIgnoreCase(s)) return true;
        }
        return false;
    }

}
