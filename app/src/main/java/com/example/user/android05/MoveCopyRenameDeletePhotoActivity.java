package com.example.user.android05;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

/**
 * @author Atmika Ponnusamy and Audri Yoon
 */

public class MoveCopyRenameDeletePhotoActivity  extends AppCompatActivity {
    private List<Album> albums = MainActivity.albums;
    private int selectedAlbum;
    private List<Photo> photos;
    private int selectedPhoto;
    private EditText newName;
    private Button rename;
    private Button delete;
    private Spinner spinner;
    private Button move;
    private Button copy;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_copy_rename_delete_photo);

        Bundle b = getIntent().getExtras();
        selectedAlbum = b.getInt("album");
        photos = albums.get(selectedAlbum).getPhotoList();
        selectedPhoto = b.getInt("photo");

        newName = (EditText) findViewById(R.id.rename_photo);
        rename = (Button) findViewById(R.id.rename);

        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photos.get(selectedPhoto).setCaption(newName.getText().toString());

                AlbumContentsActivity.updateAdapter();
                MainActivity.writeApp();

                rename.setText("");

                Intent i = new Intent(MoveCopyRenameDeletePhotoActivity.this,
                        AlbumContentsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                i.putExtras(b);
                startActivity(i);
            }
        });

        delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photos.remove(selectedPhoto);
                AlbumContentsActivity.updateAdapter();
                MainActivity.writeApp();
                Intent i = new Intent(MoveCopyRenameDeletePhotoActivity.this,
                        AlbumContentsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                i.putExtras(b);
                startActivity(i);
            }
        });


        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<Album> adapter = new ArrayAdapter<Album>(this.getApplicationContext(),
                android.R.layout.simple_spinner_item, albums);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        move = (Button) findViewById(R.id.move);
        copy = (Button) findViewById(R.id.copy);

        move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Album newAlbum = (Album) spinner.getSelectedItem();
                Photo movePhoto = photos.get(selectedPhoto);

                if(newAlbum.getPhotoList().contains(movePhoto)){
                    Toast.makeText(getApplicationContext(),
                            "This photo already exists in that album!", Toast.LENGTH_SHORT).show();
                } else {
                    newAlbum.addPhoto(movePhoto);
                    photos.remove(selectedPhoto);
                    AlbumContentsActivity.updateAdapter();
                    MainActivity.writeApp();

                    Toast.makeText(getApplicationContext(),
                            "Photo Moved!", Toast.LENGTH_SHORT).show();

                }
                Intent i = new Intent(MoveCopyRenameDeletePhotoActivity.this,
                        AlbumContentsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                i.putExtras(b);
                startActivity(i);
            }
        });

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Album newAlbum = (Album) spinner.getSelectedItem();
                Photo movePhoto = photos.get(selectedPhoto);

                if(newAlbum.getPhotoList().contains(movePhoto)){
                    Toast.makeText(getApplicationContext(),
                            "This photo already exists in that album!", Toast.LENGTH_SHORT).show();
                } else {
                    newAlbum.addPhoto(movePhoto);
                    AlbumContentsActivity.updateAdapter();
                    MainActivity.writeApp();

                    Toast.makeText(getApplicationContext(),
                            "Photo Copied!", Toast.LENGTH_SHORT).show();
                }
                Intent i = new Intent(MoveCopyRenameDeletePhotoActivity.this,
                        AlbumContentsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }
}
