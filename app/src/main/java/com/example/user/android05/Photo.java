package com.example.user.android05;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Atmika Ponnusamy and Audri Yoon
 */
public class Photo implements Serializable {
    private static final long serialVersionUID = -987644404632105556L;
//    public static Comparator<Photo> compareByDate = (Photo photo1, Photo photo2) -> photo1.getDate().compareTo(photo2.getDate());
//    public static Comparator<Photo> compareByCaption = (Photo photo1, Photo photo2) -> photo1.getCaption().toLowerCase().compareTo(photo2.getCaption().toLowerCase());

    //private Bitmap image;
    //private byte[] image;
    private int[] pixels;
    private List<Tag> tags = new ArrayList<Tag>();
    private String filename;
    private String filepath;
    private int height;
    private int width;

    public Photo(Bitmap bitmap, Intent data) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        pixels = new int [width*height];
        bitmap.getPixels(pixels,0,width,0,0,width,height);
        this.filename = new File(data.getData().getPath()).getName();
        this.filepath = data.getData().getPath();
    }

    public String getFilepath(){
        return filepath;
    }

    /**Gets the list of tags associated with this photo.
     * @return a list of tags.
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**Sets a list of tags and assigns it to a photo.
     * @param tags, a list of this photos tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**Deletes a tag from the photos list of associated tags.
     * @param tag to be deleted
     */
    public void deleteTag(Tag tag) {
        for(Tag t : tags) {
            if(t.getName().equals(tag.getName()) && t.getType().equals(tag.getType())){
                tags.remove(tag);
            }
        }
    }

    /**Adds a tag to the photo's list of tags
     * @param tag to be added
     */
    public void addTag(Tag tag) {
        if(tags.size() < 3) tags.add(tag);

    }

    /**
     * @return a String representation of a caption of a photo.
     */
    public String getCaption() {
        return filename;
    }

    /**Set a photo's caption.
     * @param caption in String form
     */
    public void setCaption(String caption) {
        this.filename = caption;
    }

    /**
     * @return an Image object of a photo
     */
//    public Image getFileAsImage() {
//        String fileLoc = image.toURI().toString();
//        return new Image(fileLoc);
//    }

    /**
     * @return a String representation of a photo's list of tags
     */
    public String getTagListString() {
        String tagsList = new String();
        for (Tag t : tags) {
            tagsList += t.getType() + ":" + t.getName() + "\n";
        }
        return tagsList;
    }

    /**
     * @return the photo's caption
     */
    public String toString() {
        return getCaption();
    }

    public Bitmap getBitmap(){
        return Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888);
    }

}
