package com.example.user.android05;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * @author: Atmika Ponnusamy and Audri Yoon
 */

public class PhotoDisplayActivity extends AppCompatActivity {

    private List<Album> albums = MainActivity.albums;
    private List<Photo> photos;
    private ImageView image;
    private TextView album;
    private TextView title;
    private TextView tag1;
    private TextView tag2;
    private TextView tag3;
    private int selectedPhoto;
    private int selectedAlbum;
    private ImageButton left;
    private ImageButton right;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_display);

        Bundle b = getIntent().getExtras();
        selectedAlbum = b.getInt("album");
        photos = albums.get(selectedAlbum).getPhotoList();
        selectedPhoto = b.getInt("photo");

        image = findViewById(R.id.image);
        album = findViewById(R.id.album);
        album.setText(getString(R.string.album) + albums.get(b.getInt("album")).getName());
        title = findViewById(R.id.title);
        tag1 = findViewById(R.id.tag1);
        tag2 = findViewById(R.id.tag2);
        tag3 = findViewById(R.id.tag3);
        setSelectedImage();

        left = (ImageButton) findViewById(R.id.left);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedPhoto != 0) selectedPhoto--;
                setSelectedImage();
            }
        });

        right = (ImageButton) findViewById(R.id.right);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedPhoto != photos.size()-1) selectedPhoto++;
                setSelectedImage();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PhotoDisplayActivity.this,
                        EditTagsActivity.class);
                Bundle b = new Bundle();
                b.putInt("album", selectedAlbum);
                b.putInt("photo", selectedPhoto);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    private void setSelectedImage() {
        Photo thisPhoto = photos.get(selectedPhoto);
        //byte[] bitmapdata = thisPhoto.getImage();
        image.setImageBitmap(thisPhoto.getBitmap());
        title.setText(thisPhoto.getCaption());
        List<Tag> tags = thisPhoto.getTags();
        for (int i = 0; i  < tags.size(); i++) {
            if(i == 0) tag1.setText(tags.get(i).toString());
            if(i == 1) tag2.setText(tags.get(i).toString());
            if(i == 2) tag3.setText(tags.get(i).toString());
        }
    }

//    public static void updatedSelectedImage(){
//        setSelectedImage();
//    }
}
