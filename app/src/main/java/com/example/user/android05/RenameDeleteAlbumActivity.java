package com.example.user.android05;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;


/**
 * @author Atmika Ponnusamy and Audri Yoon
 */
public class RenameDeleteAlbumActivity extends AppCompatActivity {

    private List<Album> albums = MainActivity.albums;
    private Button rename;
    private Button delete;
    private EditText renameAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rename_delete_album);

        Bundle b = getIntent().getExtras();
        final int index = b.getInt("index");

        rename = (Button) findViewById(R.id.rename);
        renameAlbum = (EditText) findViewById(R.id.rename_album);
        renameAlbum.setHint(albums.get(index).toString());
        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(renameAlbum.getText().toString())) {
                    renameAlbum.setError("Enter a name");
                } else if(MainActivity.hasAlbum(renameAlbum.getText().toString().trim())){
                    renameAlbum.setError("Album already exists. Enter a unique name.");
                } else {

                    String newName = renameAlbum.getText().toString().trim();
                    albums.get(index).setName(newName);

                    Toast.makeText(getApplicationContext(),
                            "Album Renamed!", Toast.LENGTH_SHORT).show();
                    MainActivity.updateAdapter();
                    MainActivity.writeApp();

                    renameAlbum.setText("");

                    Intent i = new Intent(RenameDeleteAlbumActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });

        delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                albums.remove(albums.get(index));

                Toast.makeText(getApplicationContext(),
                        "Album Deleted!", Toast.LENGTH_SHORT).show();
                MainActivity.updateAdapter();
                MainActivity.writeApp();

                Intent i = new Intent(RenameDeleteAlbumActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }

}
