package com.example.user.android05;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Atmika Ponnusamy and Audri Yoon
 */
public class SearchActivity extends AppCompatActivity {

    private Button searchBy1;
    private Button searchBy2;
    private Spinner type;
    private Spinner type1;
    private Spinner type2;
    private Spinner combo;
    private EditText tag;
    private EditText tag1;
    private EditText tag2;

    private List<Album> albums = MainActivity.albums;
    private List<Photo> results = new ArrayList<Photo>();
    private String searchSpecs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        searchBy1 = (Button) findViewById(R.id.searchby1);
        searchBy1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tag = (EditText) findViewById(R.id.tag);
                type = (Spinner) findViewById(R.id.type);

                if(TextUtils.isEmpty(tag.getText().toString())) {
                    tag.setError("Enter a tag");
                } else {
                    String targetType = type.getSelectedItem().toString();
                    String targetTag = tag.getText().toString().toLowerCase();
                    searchSpecs = targetType + " : " + targetTag;
                    for(Album a : albums) {
                        for(Photo p : a.getPhotoList()) {
                            for(Tag t : p.getTags()) {
                                if (t.getName().startsWith(targetTag) && t.getType().equals(targetType)) {
                                    results.add(p);
                                    break;
                                }
                            }
                        }
                    }

                    Album newAlbum = new Album(searchSpecs, results);
                    albums.add(newAlbum);
                    MainActivity.updateAdapter();

                    //collect search results
                    //add results to new album in albums
                    //send album to albumdisplaycontents

                    Intent intent = new Intent(SearchActivity.this,
                            AlbumContentsActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("album", albums.indexOf(newAlbum));
                    intent.putExtras(b);
                    startActivity(intent);
                }
            }
        });

        searchBy2 = (Button) findViewById(R.id.searchby2);
        searchBy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tag1 = (EditText) findViewById(R.id.tag1);
                tag2 = (EditText) findViewById(R.id.tag2);
                combo = (Spinner) findViewById(R.id.combo);
                type1 = (Spinner) findViewById(R.id.type1);
                type2 = (Spinner) findViewById(R.id.type2);

                if(!(TextUtils.isEmpty(tag1.getText().toString()) || TextUtils.isEmpty(tag2.getText().toString()))) {
                    String ty1 = type1.getSelectedItem().toString();
                    String t1 = tag1.getText().toString().toLowerCase();
                    String ty2 = type2.getSelectedItem().toString();
                    String t2 = tag2.getText().toString().toLowerCase();
                    String op = combo.getSelectedItem().toString();

                    if(op.equals("AND")) {
                        for(Album a : albums) {
                            for(Photo p : a.getPhotoList()) {
                                int count = 0;
                                for(Tag t : p.getTags()) {
                                    if ((t.getName().startsWith(t1) && t.getType().equals(ty1)) || t.getName().startsWith(t2)  && t.getType().equals(ty2)) {
                                        count++;
                                    }
                                    if(count==2) {
                                        results.add(p);
                                        break;
                                    }
                                }

                            }
                        }
                    } else {
                        for(Album a : albums) {
                            for(Photo p : a.getPhotoList()) {
                                for(Tag t : p.getTags()) {
                                    if ((t.getName().startsWith(t1) && t.getType().equals(ty1)) || t.getName().startsWith(t2)  && t.getType().equals(ty2)) {
                                        results.add(p);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    searchSpecs = ty1 + " : " + t1 + " " + op + " " + ty2 + " : " + t2;

                    Album newAlbum = new Album(searchSpecs, results);
                    albums.add(newAlbum);
                    MainActivity.updateAdapter();

                    //collect search results
                    //add results to new album in albums
                    //send album to albumdisplaycontents

                    Intent intent = new Intent(SearchActivity.this,
                            AlbumContentsActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("album", albums.indexOf(newAlbum));
                    intent.putExtras(b);
                    startActivity(intent);
                } else if(TextUtils.isEmpty(tag1.getText().toString())) {
                    tag1.setError("Enter a tag");
                } else if(TextUtils.isEmpty(tag2.getText().toString())) {
                    tag2.setError("Enter a tag");
                }
            }
        });

    }
}
