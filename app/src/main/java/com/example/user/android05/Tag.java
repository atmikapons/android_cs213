package com.example.user.android05;

import java.io.Serializable;

/**
 *
 * @author Atmika Ponnusamy and Audri Yoon
 *
 */
public class Tag implements Serializable{
    private String name;
    private String type;

    /**
     * @param type of new Tag
     * @param name of new Tag
     * Constructor for Tag with tag type and label name
     */
    public Tag(String type, String name) {
        this.name = name;
        this.type = type;
    }

    /**Gets the type of Tag
     * @return string representation of type
     */
    public String getType() {
        return type;
    }

    /**Sets type of Tag
     * @param type A String containing type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**Gets name of Tag
     * @return String representation of Tag name
     */
    public String getName() {
        return name;
    }

    /**Sets name of Tag
     * @param name Takes string representation of Tag name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return type + ": " + name;
    }


}
